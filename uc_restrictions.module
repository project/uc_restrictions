<?php
// $Id$

/**
 * @file
 * The API that contains common functions for modules that restrict the purchase of goods.
 */

/******************************************************************************
 * Drupal Hooks                                                               *
 */

/**
 * Implements hook_help().
 *
 * @param string $section
 * @return string
 */
function uc_restrictions_help($path, $arg) {
  switch ($path) {
    case 'admin/store/settings/restrictions':
      $output = '<p>' . t('This module will do its best to prevent the wrong sort of people from ordering the wrong sort of goods, or shipping of certain goods to the wrong kinds of places.');
      $output .= '<p>' . t('For example, some products cannot be ordered by minors, or some products cannot be shipped to certain states.  Or both!');
      $output .= '<p>' . t('The module was originally developed for a site that sells wine.');
      return $output;
  }
}

/**
 * Implements hook_menu().
 *
 * Called when Drupal is building menus.  Cache parameter lets module know
 * if Drupal intends to cache menu or not - different results may be
 * returned for either case.
 *
 * @param may_cache true when Drupal is building menus it will cache
 *
 * @return An array with the menu path, callback, and parameters.
 */
function uc_restrictions_menu() {
  $items = array();

  $items['admin/store/settings/restrictions'] = array(
    'title' => 'Restrictions',
    'access arguments' => array('administer store'),
    'page callback' => 'drupal_get_form',
    'description' => 'Restrict the sale of certain products.',
    'page arguments' => array('uc_restrictions_admin_settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  $items['uc_restrictions/%ctools_js/modal'] = array(
        'title' => 'Login',
        'page callback' => 'uc_restrictions_modal',
        'page arguments' => array(1),
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
  );
  
  $items['node/%/%ctools_js'] = array(
          'title' => '',
          'page callback' => 'uc_restrictions_ajax_content',
          'page arguments' => array(1,2),
          'access callback' => TRUE,
          'type' => MENU_CALLBACK,
  );

  return $items;
}

function uc_restrictions_modal($js) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_login');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Please tell us...'),
    'ajax' => TRUE,
  );

  $output = ctools_modal_form_wrapper('uc_restrictions_user_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($form_state['values']['redirect'])) {
      $output[] = ctools_ajax_command_redirect($form_state['values']['redirect']);
    }
    else {
      if (isset($form_state['values']['message'])) {
        $message = $form_state['values']['message'] . '<p><a href="#" onclick="Drupal.CTools.Modal.dismiss()">Close</a></p>';
        $output[] = ctools_modal_command_display('Thank you', $message);
      }
    }
  }
  print ajax_render($output);
  exit;
}


/**
 * Admin Settings
 *
 * @return Forms for store administrator to set configuration options.
 */
function uc_restrictions_admin_settings($form, &$form_state) {
  $jquery_plugins = drupal_get_path('module', 'uc_restrictions') . '/jquery/';
  if (!is_file($jquery_plugins . 'jquery.cookie.js')) {
    drupal_set_message(t('Could not find the jquery cookies plugin installed
        at <strong>!jquery_plugins</strong>.
        Please <a href="http://plugins.jquery.com/project/cookie">download jquery.cookie.js</a>,
        and put it into !jquery_plugins.', array('!jquery_plugins' => $jquery_plugins)), 'error');
  }

  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 0,
  );

  // adapted from tinyMce module, thank you
  $access = user_access('use PHP for settings');

  // If the visibility is set to PHP mode but the user doesn't have this block permission, don't allow them to edit nor see this PHP code
  if (variable_get('uc_restrictions_intercept', 1) == 2 && !$access) {
    $form['visibility'] = array();
    $form['visibility']['uc_restrictions_intercept'] = array(
      '#type' => 'value',
      '#value' => 2,
    );
    $form['visibility']['uc_restrictions_intercept_pages'] = array(
      '#type' => 'value',
      '#value' => variable_get('uc_restrictions_intercept_pages', ''),
    );
  }
  else {
    $options = array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are '!blog' for the blog page and !blog-wildcard for every personal blog. !front is the front page.", array('!blog' => drupal_placeholder('blog'), '!blog-wildcard' => drupal_placeholder('blog/*'), '!front' => drupal_placeholder('<front>')));

    if ($access) {
      $options[] = t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= t('If the PHP-mode is chosen, enter PHP code between !php. Note that executing incorrect PHP-code can break your Drupal site.', array('!php' => drupal_placeholder('<?php ?>')));
    }
    $form['visibility']['uc_restrictions_intercept'] = array(
      '#type' => 'radios',
      '#title' => t('Show an intrusive intercept form on specific pages'),
      '#default_value' => variable_get('uc_restrictions_intercept', 1),
      '#options' => $options,
    );
    $form['visibility']['uc_restrictions_intercept_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => variable_get('uc_restrictions_intercept_pages', ''),
      '#description' => $description,
    );
  }
  $form['visibility']['#description'] = t('An overlay form appears to the user to ask to which zone the user plans to ship.  This form is session-aware and will appear only once per session.  The purpose of this feature is to inform users of the restrictions.');
  
  return system_settings_form($form);
}


/**
* Implements hook_page_alter().
 * Render the overlay div on pages that might use it.
 * Note decision of whether it renders is client-side (so you can cache).
 *
 * @param unknown_type $page
*/
function uc_restrictions_page_alter(&$page) {
  if (_uc_restrictions_page_match()) {
    drupal_add_js(drupal_get_path('module', 'uc_restrictions') . '/jquery/jquery.cookie.js');
    $markup = theme('uc_restrictions_overlay_form');
    $page['footer']['uc_restrictions']= array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }
}

/**
 * Implements hook_node_view().
 */
function uc_restrictions_node_view($node, $view_mode = 'full') {
  if (isset($_GET['content_only']) && TRUE) {
    print drupal_render($node->content);
    exit;
  }
}

/**
 * Theme Registry
 *
 * @return array
 */
function uc_restrictions_theme() {
  return array(
    'uc_restrictions_overlay_form' => array(
      'variables' => array('height', 'width'),
    ),
  );
}
/*******************************************************************************
 * Module and Helper Functions
 */

/**
 * Return true if cart contents contain products that are restricted.
 * @param string $type use this to grab the correct taxonomy list
 * @param array $products you can get this from unserialize($form_values['cart_contents'])
 * @return boolean
 */
function uc_restrictions_contains_restricted($type, $products) {
  $restricted_tids = array();
  $restricted_shipment = FALSE;
  $tids = variable_get($type, NULL);

  if ($tids && $tids != array(0 => 0)) {
    if (!is_array($tids)) {
      $tids = (array)$tids;
    }
    foreach ($tids as $key => $tid) {
      $term = taxonomy_term_load($tid);
      $restricted_tids[] = $term->tid;
      $restricted_taxonomy_tree = taxonomy_get_tree($term->vid, $term->tid);
      foreach ($restricted_taxonomy_tree as $term) {
        $restricted_tids[] = $term->tid;
      }
    }
    
    // determine if order contains products whose nodes are associated with restricted tids
    foreach ($products as $product) {
      // get terms of the product
      // No more taxonomy_node_get_terms(): BOOO!
      $node = node_load($product->nid);
      $result = field_view_field('node', $node, 'taxonomy_catalog', array('default'));
      $terms = $result['#object']->taxonomy_catalog['und'];
      $tags = array();
      
      $product_categories = array();
      foreach($terms as $term){
        array_push($product_categories, $term['taxonomy_term']->tid);
      }

      foreach ($product_categories as $key => $tid) {
        if (in_array($tid, $restricted_tids)) {
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}


/*******************************************************************************
 * Overlay Form
 */
/**
 * FAPI definition of the overlay form presented to the user to ask qualifying questions
 * Sub-modules will add the fields
 *
 * @ingroup forms
 * @see uc_restrictions_user_form_submit()
 *
 * @return unknown
 */
function uc_restrictions_user_form($form) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 10,
  );
  $goto = ltrim(request_uri(), '/');
  $form_state['#redirect'] = $goto; // not sure why we have to do this, otherwise goes to top of the store when user passes
  return $form;
}

/**
 * Callback for submission of the user (overlay) form.  Submit actions are defined by the sub-modules and usually involve redirection to an info page.
 * Cookie is used vs. session variable, because the overlay is triggered with javascript on the client.
 * This allows drupal page caching.
 * Also this must work for a customer who logs in before checkout.
 *
 * @param unknown_type $form
 * @param unknown_type $form_values
 */
function uc_restrictions_user_form_submit($form, $form_values) {
  if (!isset($_COOKIE['uc_restrictions_overlay_seen'])) {
    setcookie('uc_restrictions_overlay_seen', '1', 0, '/');
  }
}

/**
 * Theme for Form Overlay Wrapper
 *
 * Note, client-side javascript is used to determine if overlay should appear.
 * This is to allow you to cache your pages. *
 *
 * @ingroup themeable
 *
 * @param integer $height
 * @param integer $width
 * @return string
 */
function theme_uc_restrictions_overlay_form($variables) {
  drupal_add_css('.ctools-modal-uc_restrictions {display:none}', 'inline'); // just one line, so took the easy way out.
  ctools_include('modal');
  ctools_include('ajax');
  ctools_modal_add_js();
  
  $height = $variables['0'];
  $width = $variables['1'];
  
	drupal_add_js(array(
		'uc_restrictions' => array(
		  'modalSize' => array(
			'type' => 'fixed',
			'width' => $width,
			'height' => $height
		  ),
		  'closeText' => '',
	    'closeImage' => '',
		),
	  ), 'setting');
  
  $js = "if (jQuery.cookie('uc_restrictions_overlay_seen')!=1){
  	jQuery(document).ready(function() {jQuery('.ctools-modal-uc_restrictions').click();});
  }";
  
  drupal_add_js($js, array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));

  $output = ctools_modal_text_button('restrictions', 'uc_restrictions/nojs/modal', '', 'ctools-modal-uc_restrictions');

  return $output;
}

/********************************************************************
 * Module Functions :: Private
 */
/**
 * Determine if overlay can display.
 *
 * @return
 *   TRUE if can render, FALSE if not allowed.
 */
function _uc_restrictions_page_match() {
  $page_match = FALSE;
  $intercept_type = variable_get('uc_restrictions_intercept', 1);
  if ($pages = variable_get('uc_restrictions_intercept_pages', '')) {
    // If the PHP option wasn't selected
    if ($intercept_type < 2) {
      $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
      $page_match = drupal_match_path($path, $pages);
      if ($path != $_GET['q']) {
        $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
      }
      $page_match = !($intercept_type xor $page_match);
    }
    else {
      if (module_exists('php')) {
        $page_match = php_eval($pages);
      }
    }
  }
  elseif ($intercept_type === '0') {
    $page_match = TRUE;
  }
  return $page_match;
}

/**
 * @return array
 *	A tidy array of terms to select, keyed with their tids.
 */
function _uc_restrictions_get_terms_for_select() {
  if ($vid = variable_get('uc_catalog_vid','')) {
    $dropdown_source = taxonomy_get_tree($vid);
    $dropdown_array = array('0' => '--none--');
    foreach ($dropdown_source as $item) {
      $key = $item->tid;
      $value = str_repeat ('-', $item->depth) . $item->name;
      $dropdown_array[$key] = $value;
    }
    return $dropdown_array;
  }
}

function uc_restrictions_ajax_content($nid, $js = FALSE) {
  $node = node_load($nid);
  node_build_content($node, 'full');
  
  $output = $node->content['body'][0]['#markup'];
  $title = $node->title;
  if ($js) {
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_render($title, $output);
    // above command will exit().
  }
  else {
    drupal_set_title($title);
    return $output;
  }
}

